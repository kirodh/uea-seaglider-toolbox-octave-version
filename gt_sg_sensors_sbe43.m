function data = gt_sg_sensors_sbe43(data)

%% Identify sensor(s) that need to be processed
sensorPosition = find(strcmp(data.gt_sg_settings.sensorList(:,3),'gt_sg_sensors_sbe43.m'));

%% Check for missing coefficients
% List of necessary constants
sbe43CalibrationConstants = {...
    'Soc',...
    'Foffset',...
    'o_a',...
    'o_b',...
    'o_c',...
    'o_e',...
    'Tau20',...
    'Pcor'};

sbe43TauConstants = {...
    'D1',...
    'D2'};

sbe43HysteresisConstants = {...
    'H1',...
    'H2',...
    'H3'};

% Verify necessary calibration coeffcients are present
if any(~isfield(data.gt_sg_settings,sbe43CalibrationConstants))
    gt_sg_sub_echo({'Missing the following SBE43 calibration constants in sg_calib_constants.m:',...
        sbe43CalibrationConstants{~isfield(data.gt_sg_settings,sbe43CalibrationConstants)}});
    return
end

% Check for missing Tau coefficients
if any(~isfield(data.gt_sg_settings,sbe43TauConstants))
    gt_sg_sub_echo({'Missing the following SBE43 Tau constants in sg_calib_constants.m:',...
        sbe43TauConstants{~isfield(data.gt_sg_settings,sbe43TauConstants)},...
        'Using default Tau20 with no temperature or pressure correction.'});
    D1 = 0;
    D2 = 0;
end

% Check for missing Hysteresis coefficients
if any(~isfield(data.gt_sg_settings,sbe43TauConstants))
    gt_sg_sub_echo({'Missing the following SBE43 Hysteresis constants in sg_calib_constants.m:',...
        sbe43HysteresisConstants{~isfield(data.gt_sg_settings,sbe43HysteresisConstants)},...
        'Not applying any hysteresis correction.'});
    
    doHysteresis = false;
else
    doHysteresis = true;
end


%% For each sensor needing to be processed...
for sPos = sensorPosition
    % Identify fields
    Fields = strsplit(gt_sg_sub_find_nearest_string('O2Freq',data.gt_sg_settings.sensorList{sPos,4}),'.');
    optfieldname = Fields{1};
    rawfield = Fields{2};
    gt_sg_sub_echo({['Proceeding with SBE43 oxygen calculations using O2Freq from the ',optfieldname,'.',rawfield,' column.']});
    
    for istep=gt_sg_sub_check_dive_validity(data)
        data.hydrography(istep).oxygen = gt_sg_sub_calc_oxygen;
    end
end

    function oxygen = gt_sg_sub_calc_oxygen
        
        O2Freq = data.eng(istep).(optfieldname).(rawfield);
        
        if doHysteresis
            time_orig = (data.hydrography(istep).time - data.hydrography(istep).time(1)) * 24 * 60 * 60; % Time array in seconds
            time_interp = time_orig(1):1:time_orig(end); % Making high-res 1Hz time array
            
            % Interpolate O2Freq to 1 Hz, apply filtering/smoothing in case of spikes
            O2Freq_interp = interp1(time_orig,...
                gt_sg_sub_filter(O2Freq,2,0),...
                time_interp) + data.gt_sg_settings.Foffset;
            pres_interp = interp1(time_orig,...
                gt_sg_sub_filter(data.hydrography(istep).pressure,2,0),...
                time_interp);            
            
            % Pre-allocate for speed
            O2Freq_new = O2Freq_interp;
            
            % Run hysteresis correction
            for jstep = 2:numel(O2Freq_interp)
                % Calculate hysteresis coefficients
                D = 1 + H1 * (exp(pres_interp(jstep) / H2) - 1);
                C = exp(-1 * (time_interp(jstep) - time_interp(jstep-1)) / H3);
                
                % Apply step-wise lag correction
                O2Freq_new(jstep) = (( O2Freq_interp(jstep) ...
                    + (O2Freq_new(jstep-1) * C * D )) ...
                    - (O2Freq_interp(jstep-1) * C)) ...
                    ./ D;
            end
            
            % Downsample back to original time array
            O2Freq = interp1(time_interp, ...
                O2Freq_new - data.gt_sg_settings.Foffset, ...
                time_orig);
        end
        
        % Benson and Krause coefficients (1984) for umol.kg output (recommended for oceanographic work)
        A0 = 5.80871;
        A1 = 3.20291;
        A2 = 4.17887;
        A3 = 5.10006;
        A4 = -9.86643-2;
        A5 = 3.80369;
        B0 = -7.01577e-3;
        B1 = -7.70028e-3;
        B2 = -1.13864e-2;
        B3 = -9.51519e-3;
        C0 = -2.75915e-7;
        
%         % Garcia and Gordon coefficients (1984) for ml.L output
%         A0 = 2.00907;
%         A1 = 3.22014;
%         A2 = 4.0501;
%         A3 = 4.94457;
%         A4 = -0.256847;
%         A5 = 3.88767;
%         B0 = -6.24523e-3;
%         B1 = -7.37617e-3;
%         B2 = -1.0341e-2;
%         B3 = -8.17083e-3;
%         C0 = -4.88682e-7;
        
        % Calculate scaled temperature
        Ts = log((298.15 - data.hydrography(istep).temp)./(273.15 + data.hydrography(istep).temp));
        
        % Calculate oxygen solubility
        lnC = A0 + Ts.*(A1 + Ts.*(A2 + Ts.*(A3 + Ts.*(A4 + A5.*Ts)))) +...
            data.hydrography(istep).salinity .*(B0 + Ts.*(B1 + Ts.*(B2 + B3.*Ts))) + ...
            C0 .* data.hydrography(istep).salinity .* data.hydrography(istep).salinity;
        solub = exp(lnC);
    
        % Calculate tau as function of T and P
        tauTP = data.gt_sg_settings.Tau20 .* exp(...
            D1.* data.hydrography(istep).pressure + ...
            D2.*(data.hydrography(istep).temp - 20)...
            );
        
        % Calculate dFreq / dtime
        dFdT = gt_sg_sub_diff({
            O2Freq, ... % Raw oxygen
            (data.hydrography(istep).time - data.hydrography(istep).time(1)) * 24 * 60 * 60 ... % Time array in seconds
            });
        
        % Calculate corrected frequency
        freqCorr = O2Freq ...
            + data.gt_sg_settings.Foffset ...
            + (tauTP .* dFdT);
            
        % Calculate delta-p
        dp = (1.0 + ...
            (data.gt_sg_settings.o_a .* data.hydrography(istep).temp) + ...
            (data.gt_sg_settings.o_b .* (data.hydrography(istep).temp.^2)) + ...
            (data.gt_sg_settings.o_c .* (data.hydrography(istep).temp.^3)));
        
        % Calculate what looks like a pressure correction
        presCorr = exp(...
            data.gt_sg_settings.o_e ...
            .* data.hydrography(istep).pressure ...
            ./ (273.15 + data.hydrography(istep).temp));
        
        % Calculate oxygen concentration
        oxygen = ( data.gt_sg_settings.Soc .* freqCorr )...
            .* solub ...
            .* dp ...
            .* presCorr;
               
    end % gt_sg_sub_calc_oxygen.m
end % gt_sg_sensors_sbe43.m