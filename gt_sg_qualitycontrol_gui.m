function data = gt_sg_qualitycontrol_gui(data,variableList)
%
% Inputs:
% data = standard data structure from toolbox
% variableList (optional) = defines which variables to make available (used
%      in the first pass before everything else is calculated)
%
% Outputs:
% data.flag = substructure with the same format as .hydrography containing
%      data flags (detailed further)
% data.gt_sg_settings.QC = structure containing coordinates of flag masks
%      (polygons indicating good and bad data) as well as min and max
%      values.
%
% Flag formats:
%    0 = good data
%    1 = bad data flagged by the GUI
%
% To manually set persistent flags that do not get overwritten by the
% toolbox, make them negative (ie. -27).
%
%
% B.Y.QUESTE Jan 2017

dives = gt_sg_sub_check_dive_validity(data);

yaxisList = fieldnames(data.hydrography);
yaxisList = sort(yaxisList(...
    cellfun(@(x) ...
    all(size(data.hydrography(dives(1)).(x)) == size(data.hydrography(dives(1)).time)),...
    yaxisList)));
if nargin == 1
    variableList = yaxisList;
end

% Load existing flags
if isfield(data.gt_sg_settings,'QC')
    QC = data.gt_sg_settings.QC;
else
    QC = struct('flag',struct,'settings',struct,'masks',struct,'saved',struct);
end
if ~isfield(data,'flag')
    data.flag = struct;
end
for fstep=1:numel(yaxisList)
    if ~isfield(data.flag,yaxisList{fstep})
        tmp = arrayfun(@(x) false(size(x.(yaxisList{fstep}))),data.hydrography,'Uni',0);
        [data.flag(1:numel(data.hydrography),1).(yaxisList{fstep})] = tmp{:};
    end
end
if isfield(data,'flag')
    QC.flag = data.flag;
end

% Insert blank settings if not predefined
for fstep=1:numel(yaxisList)
    QC.saved.(yaxisList{fstep}) = 1;
    if ~isfield(QC.masks,yaxisList{fstep})
        QC.masks.(yaxisList{fstep}) = struct;
    end
    if ~isfield(QC.settings,yaxisList{fstep})
        QC.settings.(yaxisList{fstep}) = struct;
    end
    if ~isfield(QC.flag,yaxisList{fstep})
        blankFlags(yaxisList{fstep});
    end
end

% TODO: rename QC structure to QC.var.element throughout

sampleNum = arrayfun(@(x) [1:numel(x.time)],data.hydrography,'Uni',0);
diveNum = arrayfun(@(x) ones(size(x.elaps_t))*x.dive,data.eng,'Uni',0);

%% Create panel
close all;
%h_gui = figure('Visible','on');
% Set options
%set(h_gui,...
%    'MenuBar','none',...
%    'Units','normalized',...
%    'Position',[0.05 0.05 0.9 0.9]...
%    );

%% Save buttons
%kiro
%h_save = uicontrol(h_gui,'Style','pushbutton','String','Save',...
%    'Units','normalized','Position',[0.86 0.945 0.13 0.05],...
%    'CallBack',@button_save);

% uicontrol(h_gui,'Style','pushbutton','String','DEBUG',...
%    'Units','normalized','Position',[0.86 0.89 0.13 0.05],...
%    'CallBack',@keyboard_debug);
%    function keyboard_debug(~,~)
%        keyboard
%    end

%% Display flagged samples?
%kiro, set to 0 for nothing and 1 for something
h_showFlag = 0
%uicontrol(h_gui,'Style','checkbox','String','Show flagged samples?',...
%    'Units','normalized','Position',[0.72 0.93 0.13 0.03],...
%    'Value',1,...
%    'CallBack',@refreshPlots);

%% Variable Selection
%kiro
%uicontrol(h_gui,'Style','text','String','Variable',...
%    'Units','normalized','Position',[0.01 0.96 0.10 0.03]);
%kiro
h_variable = 1 %variableList
%h_variable = uicontrol(h_gui,'Style','popup','String',variableList,'Value',1,...
%    'Min',1,'Max',1,'Units','normalized','Position',[0.01 0.92 0.10 0.03],...
%    'CallBack',@changeVariable);

%% Profile y-axis dimension
%kiro
%uicontrol(h_gui,'Style','text','String','Y Axis',...
%    'Units','normalized','Position',[0.12 0.96 0.10 0.03]);
h_yaxisvar = find(strcmp('depth',yaxisList))
%h_yaxisvar = uicontrol(h_gui,'Style','popup','String',yaxisList,'Value',find(strcmp('depth',yaxisList)),...
%    'Units','normalized','Position',[0.12 0.92 0.10 0.03],...
%    'CallBack',@doProfile);

%% Color by variable
%kiro
%uicontrol(h_gui,'Style','text','String','Colour by',...
%    'Units','normalized','Position',[0.23 0.96 0.10 0.03]);
h_colorvar = 1 %uicontrol(h_gui,'Style','popup','String',{'None (faster)',yaxisList{:}},'Value',1,...
%    'Units','normalized','Position',[0.23 0.92 0.10 0.03],...
%    'CallBack',@doProfile);

%% Invert y-axis? & Line Style
h_invertYAxis = 1   %uicontrol(h_gui,'Style','checkbox','String','Invert Y-axis?',...
%    'Units','normalized','Position',[0.36 0.96 0.10 0.03],...
%    'Value',1,...
%    'CallBack',@invertYAxis);

h_showLine = 0  %uicontrol(h_gui,'Style','checkbox','String','Show line?',...
%    'Units','normalized','Position',[0.36 0.93 0.10 0.03],...
%    'Value',0,...
%    'CallBack',@doProfile);

h_markerSize = 0 %uicontrol(h_gui,'Style','checkbox','String','Large markers?',...
%    'Units','normalized','Position',[0.36 0.90 0.10 0.03],...
%    'Value',0,...
%    'CallBack',@doProfile);

%% Cut off thresholds
%uicontrol(h_gui,'Style','text','String','Minimum',...
%    'Units','normalized','Position',[0.48 0.96 0.10 0.03]);
h_min = [] %uicontrol(h_gui,'Style','edit','String',[],...
%    'Units','normalized','Position',[0.48 0.92 0.10 0.03],...
%    'CallBack',@refreshPlots);

%uicontrol(h_gui,'Style','text','String','Maximum',...
%    'Units','normalized','Position',[0.59 0.96 0.10 0.03]);
h_max = [] %uicontrol(h_gui,'Style','edit','String',[],...
%    'Units','normalized','Position',[0.59 0.92 0.10 0.03],...
%    'CallBack',@refreshPlots);

%% Dive Selection
%uicontrol(h_gui,'Style','text','String','Dive numbers',...
%    'Units','normalized','Position',[0.01 0.86 0.10 0.03]);
h_list_dives = indicesIn(dives,dives) %uicontrol(h_gui,'Style','listbox','String',dives,...
%    'Units','normalized','Position',[0.01 0.09 0.10 0.76],...
%    'Max',10000,'Min',1,'Value',indicesIn(dives,dives),...
%    'CallBack',@refreshPlots);
%uicontrol(h_gui,'Style','pushbutton','String','Select all',...
%    'Units','normalized','Position',[0.01 0.05 0.10 0.03],...
%    'CallBack',@(x,y) cellfun(@(z)feval(z),...
%    {@(x,y) h_list_dives = 1:numel(dives),...
%    @refreshPlots}));
%uicontrol(h_gui,'Style','pushbutton','String','Select none',...
%    'Units','normalized','Position',[0.01 0.01 0.10 0.03],...
%    'CallBack',@(x,y) cellfun(@(z)feval(z),...
%    {@(x,y) h_list_dives = [],...
%    @refreshPlots}));

%% Visible flags
%uicontrol(h_gui,'Style','text','String','Show merged flags for',...
%    'Units','normalized','Position',[0.67 0.42 0.11 0.03]);
h_which_flags = [1:numel(variableList)] % uicontrol(h_gui,'Style','listbox','String',variableList,...
%    'Units','normalized','Position',[0.67 0.01 0.11 0.41],...
%    'Max',10000,'Min',1,'Value',[1:numel(variableList)],...
%    'CallBack',@refreshPlots);

%% Brush buttons
%uicontrol(h_gui,'Style','pushbutton','String','2-click Zoom',...
%    'Units','normalized','Position',[0.54 0.37 0.11 0.06],...
%    'CallBack',@activateZoom);
%uicontrol(h_gui,'Style','pushbutton','String','Reset view',...
%    'Units','normalized','Position',[0.54 0.30 0.11 0.06],...
%    'CallBack',@resetAxis);
h_brush_bad = 'Flag (bad)' %uicontrol(h_gui,'Style','pushbutton','String','Flag (bad)',...
%    'Units','normalized','Position',[0.54 0.23 0.11 0.06],...
%    'CallBack',@brush_new_bad);
h_brush_good = 'Flag (good)' %uicontrol(h_gui,'Style','pushbutton','String','Flag (good)',...
%    'Units','normalized','Position',[0.54 0.16 0.11 0.06],...
%    'CallBack',@brush_new_good);
h_brush_undo = 'Delete last polygon' %uicontrol(h_gui,'Style','pushbutton','String','Delete last polygon',...
%    'Units','normalized','Position',[0.54 0.09 0.11 0.06],...
%    'CallBack',@brush_undo,'Enable','on');
h_brush_apply = 'Clear all polygons' %uicontrol(h_gui,'Style','pushbutton','String','Clear all polygons',...
%    'Units','normalized','Position',[0.54 0.02 0.11 0.06],...
%    'CallBack',@brush_clear,'Enable','on');

%% Profiles

%h_prof = axes('YAxisLocation','left','Box','on',...
%    'Units','normalized','Position',[0.16 0.04 0.35 0.84],...
%    'XGrid','on','YGrid','on',...
%    'YDir','reverse',...
%    'ButtonDownFcn','dummy',...
%    'Parent',h_gui);
% h_prof_xmin = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.15 0.08 0.03 0.03],...
%     'CallBack',@changeAxes);
% h_prof_xmax = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.49 0.08 0.03 0.03],...
%     'CallBack',@changeAxes);
% h_prof_ymin = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.13 0.865 0.03 0.03],...
%     'CallBack',@changeAxes);
% h_prof_ymax = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.13 0.105 0.03 0.03],...
%     'CallBack',@changeAxes);

%% Histogram
%h_hist = axes('YAxisLocation','right','Box','on',...
%    'Units','normalized','Position',[0.55 0.50 0.40 0.38],...
%    'XGrid','on','YGrid','on',...
%    'Parent',h_gui);
% h_hist_xmin = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.54 0.47 0.03 0.03],...
%     'CallBack',@changeAxes);
% h_hist_xmax = uicontrol(h_gui,'Style','edit','String',[],...
%     'Units','normalized','Position',[0.93 0.47 0.03 0.03],...
%     'CallBack',@changeAxes);

%% Initialise
% Dives and variables
selectedDives = dives;
%kiro
%variable = variableList{get(h_variable,'Value')};
variable = variableList{h_variable};
%%% kiro put this in for the saving of the flags
button_save


% Polygon initialisation
h_brush_num = nan;
h_brush_color = [1 0 0];
h_brush_poly = []; h_brush_marker = [];

% Setup data.flag substructure
if ~isfield(data,'flag')
    data.flag(1:numel(data.hydrography),1) = struct;
end
for fstep=1:numel(yaxisList)
    if ~isfield(data.flag(dives(1)),yaxisList{fstep})
        for dstep = 1:numel(data.hydrography)
            data.flag(dstep,1).(yaxisList{fstep}) = false(size(data.hydrography(dstep).(yaxisList{fstep})));
        end
    end
end
restoreFlagsSettings;

changeVariable(0,0);

%% GENERAL SUBFUNCTIONS

    function changeVariable(~,~)
        % Initialises GUI for a set variable
        if ~QC.saved.(variable)
            choice = questdlg({[variable,' flags not saved.'],'Would you like to proceed'}, ...
                'Flags not saved', ...
                'Yes','Cancel','Cancel');
            % Handle response
            if ~strcmp(choice,'Yes')
                %kiro
                %set(h_variable,'Value',variableList{strcmp(variableList,variable)});
                h_variable = variableList{strcmp(variableList,variable)}
                return;
            end
        end
        % kiro
        %variable = variableList{get(h_variable,'Value')};
        variable = variableList{h_variable};
% kiro
      %  set(h_save,'String',['Save ',variable]);

        restoreFlagsSettings;

        % Remove possible polygons
        try; delete(h_brush_poly); end;
        h_brush_poly = [];

        h_which_flags = [1:numel(variableList)];

        %cla(h_prof)
        refreshPlots;
        %resetAxis(0,0);
    end

    function changeYVariable(~,~)
    %kiro
        yaxisvar = h_yaxisvar;
        if ~isfield(QC.masks.(variable),yaxisList{yaxisvar})
            QC.masks.(variable).(yaxisList{yaxisvar}).x_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar}).y_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar}).value = [];
            QC.masks.(variable).(yaxisList{yaxisvar}).dives = {};
        end
        refreshPlots;
    end

    function button_save(~,~)
        saveFlags(variable);
        saveSettings(variable);
        gt_sg_sub_echo({['Saving flag data for ',variable,'.']});
        QC.saved.(variable) = 1;
    end

%% DYNAMIC FUNCTION CALLER TO MODIFY CLICK FUNCTION

profileButtonFcn = @(~,~) 0;

    function profileButtonFcnCaller(~,~)
        profileButtonFcn(0,0);
    end

%% FLAGS AND EXPORT SETTINGS SUBFUNCTIONS

    function refreshPlots(~,~)
        % Replots everything anytime there is a change.
        selectedDives = dives(h_list_dives);

        % Mark bad values, the order is important here, persistend flags
        % need to be reinstated last.
        blankFlags(variable);
        applyCutoffs
        applyPolygons
        applyPersistentFlags

        % Draw the plots based on new info
        %doProfile;
        %doHistogram;

        % Indicate changes have occured if the user tries to change
        % variables:
        QC.saved.(variable) = 0;
    end

    function applyCutoffs
        % If min|max set, then set flags to false where they are currently
        % true and values exceed limits.
        if ~isempty(h_min)
            tmp = arrayfun(@(x,y) (x.(variable) < str2double(h_min)) | y.(variable),...
                data.hydrography(selectedDives),QC.flag(selectedDives),'Uni',0);
            [QC.flag(selectedDives).(variable)] = tmp{:};
        end
        if ~isempty(h_max)
            tmp = arrayfun(@(x,y) (x.(variable) > str2double(h_max)) | y.(variable),...
                data.hydrography(selectedDives),QC.flag(selectedDives),'Uni',0);
            [QC.flag(selectedDives).(variable)] = tmp{:};
        end
    end

    function applyPolygons
        yaxisvar_value = h_yaxisvar;
        if ~isfield(QC.masks.(variable),yaxisList{yaxisvar_value})
            QC.masks.(variable).(yaxisList{yaxisvar_value}).x_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar_value}).y_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar_value}).value = [];
            QC.masks.(variable).(yaxisList{yaxisvar_value}).dives = {};
        end

        fields = fieldnames(QC.masks.(variable));
        for polystep = 1:numel(fields)

            if isfield(QC.masks.(variable).(fields{polystep}),'value')
                good_poly = find(QC.masks.(variable).(fields{polystep}).value == 0);
                bad_poly = find(QC.masks.(variable).(fields{polystep}).value == 1);

                for pstep = [bad_poly,good_poly] % bad, then good to overwrite
                    if ~isempty(QC.masks.(variable).(fields{polystep}).x_brush{pstep})
                        in = inpolygon(...
                            [data.hydrography(...
                            QC.masks.(variable).(fields{polystep}).dives{pstep}...
                            ).(variable)],...
                            [data.hydrography(...
                            QC.masks.(variable).(fields{polystep}).dives{pstep}...
                            ).(fields{polystep})],...
                            QC.masks.(variable).(fields{polystep}).x_brush{pstep}, ...
                            QC.masks.(variable).(fields{polystep}).y_brush{pstep} ...
                            );

                        dnum = subsref([diveNum{...
                            QC.masks.(variable).(fields{polystep}).dives{pstep}...
                            }],struct('type','()','subs',{{find(in)}}));
                        snum = subsref([sampleNum{...
                            QC.masks.(variable).(fields{polystep}).dives{pstep}...
                            }],struct('type','()','subs',{{find(in)}}));


                        %1 = bad. 0 = good.

                        for dstep = unique(dnum)
                            QC.flag(dstep).(variable)(snum(dnum == dstep)) = ...
                                ismember(pstep,bad_poly) | data.flag(dstep).(variable)(snum(dnum == dstep));
                        end
                    end
                end
            end
        end
    end

    function applyPersistentFlags
        % A bit complicated... Example:
        % QC      = [ 0 0 0   0 0 1]; QC working structure
        % FL      = [ 0 1 0 -27 0 0]; data.flag "saved" structure
        % Desired = [ 0 0 0 -27 0 1];
        % Solution: (QC | (FL.*(FL<0))) .* ((FL>=0)+(FL.*(FL<0)))
        %
        % (FL.*(FL<0)) only outputs negative flags and zeros
        % +(FL>=0) so as to turn the zeros into ones, while not modifying
        % negative flags so that when we multiply, we preserve the previous
        % booleon values.
        %
        % Keep new flags in QC as we have just done the polygons and limits,
        % and only preserve the *negative* flags from the data.flag
        % structure
        tmp = arrayfun(@(QC,FL) ...
            (QC.(variable) | (FL.(variable).*(FL.(variable)<0))) .* ...
            ((FL.(variable)>=0)+(FL.(variable).*(FL.(variable)<0))) , ...
            ...
            QC.flag(selectedDives),data.flag(selectedDives),'Uni',0);

        [QC.flag(selectedDives).(variable)] = tmp{:};
    end

    function blankFlags(var)
            tmp = arrayfun(@(x) x.(var) & false,data.flag,'Uni',0);
            [QC.flag(1:numel(data.hydrography),1).(var)] = tmp{:};
    end

    function saveFlags(var)
        [data.flag(:).(var)] = QC.flag(1:numel(data.hydrography)).(var);
    end

    function saveSettings(var)

        %xlim_h_prof = get(h_prof,'XLim');
        %ylim_h_prof = get(h_prof,'YLim');
        %xlim_h_hist = get(h_prof,'XLim');

        data.gt_sg_settings.QC.settings.(var).min = str2double(h_min);
        data.gt_sg_settings.QC.settings.(var).max = str2double(h_max);
        data.gt_sg_settings.QC.masks.(variable) = QC.masks.(variable);
        %QC.prof_XLim = xlim_h_prof;
        %QC.prof_YLim = ylim_h_prof;
        %QC.hist_XLim = xlim_h_hist;
    end

    function restoreFlagsSettings
        h_min='NaN'
        h_max='NaN'

        % Restore saved values if exist
        for dstep = 1:numel(data.hydrography)
            QC.flag(dstep).(variable) = data.flag(dstep).(variable);
        end
        try
            h_min = data.gt_sg_settings.QC.settings.(variable).min
        end
        try
            h_max = data.gt_sg_settings.QC.settings.(variable).max
        end
        try
            QC.masks.(variable) = data.gt_sg_settings.QC.masks.(variable);
        end
    end

%% BRUSH/POLYGON SUBFUNCTIONS

    function brush_undo(~,~)
        if numel(h_brush_poly) > 0
            try; delete(h_brush_poly(end)); end;
            yaxisvar = h_yaxisvar;
            if numel(h_brush_poly) == 1
                h_brush_poly = [];
                QC.masks.(variable).(yaxisList{yaxisvar}).x_brush = {};
                QC.masks.(variable).(yaxisList{yaxisvar}).y_brush = {};
                QC.masks.(variable).(yaxisList{yaxisvar}).value = [];
                QC.masks.(variable).(yaxisList{yaxisvar}).dives = {};
            else
                h_brush_poly = h_brush_poly(1:end-1);
                QC.masks.(variable).(yaxisList{yaxisvar}).x_brush = ...
                    QC.masks.(variable).(yaxisList{yaxisvar}).x_brush(1:end-1);
                QC.masks.(variable).(yaxisList{yaxisvar}).y_brush = ...
                    QC.masks.(variable).(yaxisList{yaxisvar}).y_brush(1:end-1);
                QC.masks.(variable).(yaxisList{yaxisvar}).value = ...
                    QC.masks.(variable).(yaxisList{yaxisvar}).value(1:end-1);
                QC.masks.(variable).(yaxisList{yaxisvar}).dives = ...
                    QC.masks.(variable).(yaxisList{yaxisvar}).dives{1:end-1};
            end

            refreshPlots(0,0);
        end
    end
%Kiro
%    function brush_save(~,~)
%        h_brush_num = nan;
%        set(h_brush_poly(end),...
%            'LineStyle', ':');
%        try; delete(h_brush_marker); end;
%
%        refreshPlots(0,0);
%    end

    function brush_new_good(~,~)
        [h_brush_bad] = 'off';
        [h_brush_apply,h_brush_undo] = 'off';
        [h_brush_good] = 'Save polygon';

        h_brush_color = [0 0.5 1];
        %brush_new;

        [h_brush_bad] = 'on';
        [h_brush_apply,h_brush_undo] = 'on';
        [h_brush_good] = 'Flag (good)';
    end

    function brush_new_bad(~,~)
        [h_brush_good] = 'off';
        [h_brush_apply,h_brush_undo] = 'off';
        [h_brush_bad] = 'Save polygon';

        h_brush_color = [1 0 0];

        %brush_new;

        [h_brush_good] = 'on';
        [h_brush_apply,h_brush_undo] = 'on';
        [h_brush_bad] = 'Flag (bad)';
    end

    % function not used
    function brush_new
        h_brush_num = 0;
        hold(h_prof,'on');
        yaxisvar = h_yaxisvar;
        if ~isfield(QC.masks.(variable),yaxisList{yaxisvar})
            QC.masks.(variable).(yaxisList{yaxisvar}).x_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar}).y_brush = {};
            QC.masks.(variable).(yaxisList{yaxisvar}).value = [];
            QC.masks.(variable).(yaxisList{yaxisvar}).dives = {};
        end
        QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end+1} = [];
        QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end+1} = [];
        QC.masks.(variable).(yaxisList{yaxisvar}).value(end+1) = ...
            (h_brush_color(1) == 1);
        QC.masks.(variable).(yaxisList{yaxisvar}).dives{end+1} = selectedDives;

        h_brush_poly(end+1) = line('Parent', h_prof,...
            'XData', QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end},...
            'YData', QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end},...
            'Visible', 'on',...
            'Clipping', 'on',...
            'Color', h_brush_color,...
            'LineStyle', '-',...
            'LineWidth',1,'ButtonDownFcn','@profileButtonFcnCaller');

        %set(h_prof,'ButtonDownFcn',@brush_drawPoly_drawPoint);
        profileButtonFcn = @(~,~) 0; %@brush_drawPoly_drawPoint;

        set(gcf,'Pointer', 'crosshair');

        while ~isnan(h_brush_num)
            pause(0.05);
            %disp('pausing');
        end

        profileButtonFcn = @(~,~) 0;
        set(gcf,'Pointer', 'arrow');
        hold(h_prof,'off');

        % function not in use
        function brush_drawPoly_drawPoint(~,~)
            if h_brush_num ~= -1
                pt = get(h_prof, 'CurrentPoint');
                x = pt(1,1);
                y = pt(1,2);

                %xlim_h_prof = get(h_prof,'XLim');
                %ylim_h_prof = get(h_prof,'YLim');

                yaxisvar = h_yaxisvar;

                if ~any([...
                        x < xlim_h_prof(1), x > xlim_h_prof(2), ...
                        y < ylim_h_prof(1), y > ylim_h_prof(2) ])

                    QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end}(h_brush_num+1) = x;
                    QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end}(h_brush_num+1) = y;
                    QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end}(h_brush_num+2) = ...
                        QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end}(1);
                    QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end}(h_brush_num+2) = ...
                        QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end}(1);

                    set(h_brush_poly(end),...
                        'XData', QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end},...
                        'YData', QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end});

                    if h_brush_num == 0
                        hold(h_prof,'on');
                        h_brush_marker = scatter(...
                            QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{end}(1), ...
                            QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{end}(1), ...
                            14,h_brush_color,...
                            'Parent',h_prof);
                        hold(h_prof,'off');
                    end

                    h_brush_num = h_brush_num+1;
                end
            end
        end

    end

    function brush_clear(~,~)
        fields = fieldnames(QC.masks.(variable));
        for polystep = 1:numel(fields)
            QC.masks.(variable).(fields{polystep}).value = [];
            QC.masks.(variable).(fields{polystep}).x_brush = {};
            QC.masks.(variable).(fields{polystep}).y_brush = {};
            QC.masks.(variable).(fields{polystep}).dives = {};
        end

        refreshPlots(0,0);
    end

%% PLOTTING SUBFUNCTIONS

    % function not used
    function doHistogram
        cla(h_hist);
        % kiro, h_showFlag is a 0 (or 1 untested)
        if h_showFlag  %get(h_showFlag,'Value')
            dataArray = [data.hydrography(selectedDives).(variable)];
        else
            dataArray = indexarray([data.hydrography(selectedDives).(variable)],~[QC.flag(selectedDives).(variable)]);
        end
        set(h_hist,'NextPlot','add');
        hist(h_hist,dataArray,1000);

        tmp = nanmean(dataArray);
        tmp2 = nanstd(dataArray);

        %xlim_h_hist = get(h_hist,'XLim');
        ylim_h_hist = get(h_hist,'YLim');

        plot(h_hist,[tmp tmp],ylim_h_hist,':k','LineWidth',1);
        plot(h_hist,[tmp-tmp2 tmp-tmp2],ylim_h_hist,':r','LineWidth',1);
        plot(h_hist,[tmp+tmp2 tmp+tmp2],ylim_h_hist,':r','LineWidth',1);

        set(h_hist,'XGrid','on','YGrid','on','NextPlot','replace');
    end
    % function not used
    function doProfile(~,~)
        flaggedValues = false(size([QC.flag(selectedDives).(variable)]));
        fields = variableList(h_which_flags);

        LineStyle = {'none','-'};
        MarkerSize = [2,7];

        %         flaggedValues = flaggedValues | ...
        %             [QC.flag(selectedDives).(variable)] | ...
        %             [QC.flag(selectedDives).(yaxisList{h_yaxisvar.Value})];

        for fstep = 1:numel(fields)
            flaggedValues = flaggedValues |  [QC.flag(selectedDives).(fields{fstep})];
        end
        %kiro
        if h_colorvar-1 > 0
            colorVar = yaxisList{h_colorvar-1}; % +1 to colorvar to account for the 'none' line
        else
            colorVar = 'time';
        end

        % kiro
        if h_showFlag  %get(h_showFlag,'Value')
            rapidScatter(...
                indexarray([data.hydrography(selectedDives).(variable)],~flaggedValues),...
                indexarray([data.hydrography(selectedDives).(yaxisList{h_yaxisvar})],~flaggedValues),...
                indexarray([data.hydrography(selectedDives).(colorVar)],~flaggedValues));
            %set(h_prof,'NextPlot','add');

            %plot(h_prof,...
            %    indexarray([data.hydrography(selectedDives).(variable)],flaggedValues),...
            %    indexarray([data.hydrography(selectedDives).(yaxisList{h_yaxisvar})],flaggedValues),...
            %    'r','LineStyle','none','Marker','x','Color',[1 0.5 0.5],'MarkerSize',1,'ButtonDownFcn','@profileButtonFcnCaller');
            %set(h_prof,'NextPlot','replace');
        else
            rapidScatter(...
                indexarray([data.hydrography(selectedDives).(variable)],~flaggedValues),...
                indexarray([data.hydrography(selectedDives).(yaxisList{h_yaxisvar})],~flaggedValues),...
                indexarray([data.hydrography(selectedDives).(colorVar)],~flaggedValues));
        end

        %drawPolygons;
        %invertYAxis(0,0);
        % not used
        function rapidScatter(x,y,v)
            is_held = ishold;
            if ~is_held
                cla(h_prof)
            end
            hold(h_prof,'on');

            if h_colorvar == 1
            'dummy line'
                %line('Parent',h_prof,'XData',x(~isnan(x+y)),'YData',y(~isnan(x+y)),'LineStyle',LineStyle{h_showLine + 1},'Color',[0.65 0.65 0.65],'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'MarkerSize',MarkerSize(h_markerSize + 1),'Marker','.','LineWidth',0.5,'ButtonDownFcn','@profileButtonFcnCaller');
            else
                if ~isempty(v)
                    cmap = winter(10);
                    [~, in_bin] = histc(v,linspace(min(v)-eps,max(v)+eps,length(cmap)+1));
                    if h_showLine
                    'dummy lin 1'
                        %line('Parent',h_prof,'XData',x(~isnan(x+y)),'YData',y(~isnan(x+y)),'LineStyle','-','Color',[0.65 0.65 0.65],'ButtonDownFcn','@profileButtonFcnCaller');
                    end
                    for rstep = randperm(max(in_bin))
                        try
                        'try plot'
                            %plot(h_prof,x(in_bin == rstep),y(in_bin == rstep),'o','MarkerSize',MarkerSize(h_markerSize + 1),'LineStyle','none','Color',cmap(rstep,:),'MarkerFaceColor',cmap(rstep,:),'ButtonDownFcn','@profileButtonFcnCaller');
                        end
                    end
                else
                    cla(h_prof);
                end
            end
            if ~is_held
                hold off
            end
        end

    end

    % function not used
    function drawPolygons
        hold(h_prof,'on');
        yaxisvar = h_yaxisvar;
        if isfield(QC.masks.(variable),yaxisList{yaxisvar})
            if ~isempty(QC.masks.(variable).(yaxisList{yaxisvar}).x_brush)
                h_brush_poly = [];

                for bstep = 1:numel(QC.masks.(variable).(yaxisList{yaxisvar}).x_brush)

                    switch QC.masks.(variable).(yaxisList{yaxisvar}).value(bstep)
                        case 0
                            h_brush_color = [0 0.5 1];
                        case 1
                            h_brush_color = [1 0 0];
                    end

                    h_brush_poly(bstep) = 1 %line('Parent', h_prof,...
                        %'XData', QC.masks.(variable).(yaxisList{yaxisvar}).x_brush{bstep},...
                        %'YData', QC.masks.(variable).(yaxisList{yaxisvar}).y_brush{bstep},...
                        %'Visible', 'on',...
                        %'Clipping', 'on',...
                        %'Color', h_brush_color,...
                        %'LineStyle', ':',...
                        %'LineWidth',1,'ButtonDownFcn','@profileButtonFcnCaller');
                end
            end
        end
        hold(h_prof,'off');
    end

%% AXIS ADJUSTMENT SUBFUNCTIONS

    function activateZoom(~,~)
        profileButtonFcn =@(~,~) 0;% @profileZoom;
        %set(h_prof,'ButtonDownFcn',@profileZoom);
        set(gcf,'Pointer', 'cross');
    end

    function profileZoom(~,~)
        function profileZoom_up(~,~)
            pt = get(h_prof, 'CurrentPoint');
            delete(h_scatter);
            profileButtonFcn = @(~,~) 0;
            set(h_prof,'YLim',sort([y pt(1,2)+eps]),'XLim',sort([x pt(1,1)+eps]));
            set(gcf,'Pointer', 'arrow');
        end
        pt = get(h_prof, 'CurrentPoint');
        profileButtonFcn = @(~,~) 0;% @profileZoom_up;
        x = pt(1,1);
        y = pt(1,2);
        is_held = ishold;
        if ~is_held
            hold(h_prof,'on');
        end
        h_scatter = scatter(x,y,30,[1 0 0],'Marker','x');
        if ~is_held
            hold(h_prof,'on');
        end
    end

    function resetAxis(~,~)
        axis(h_prof,'tight');
        axlim = axis(h_prof);
        axis(h_prof,axlim + [-(axlim(2)-axlim(1))*0.007 +(axlim(2)-axlim(1))*0.007 -(axlim(4)-axlim(3))*0.007 +(axlim(4)-axlim(3))*0.007]);
    end

    function invertYAxis(~,~)
        if h_invertYAxis
            set(h_prof,'YDir','reverse');
        else
            set(h_prof,'YDir','normal');
        end
    end

%% UTILITY SUBFUNCTIONS (INDEXING ETC)

    function out = indicesIn(list,sublist)
        [~, out, ~] = intersect(list,sublist);
    end

    function out = indexarray(array,flags)
        % I like this one :)
        out = ...
            subsref(array,...
            struct('type','()','subs', {{ find(flags) }} ));
    end

%% WAIT FOR GUI CLOSE
%uiwait(h_gui)
%keyboard
end
